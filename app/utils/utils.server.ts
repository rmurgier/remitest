import { v4 as uuid } from 'uuid'

export const createId = () => uuid()
export const randomArrayElement = <T>(arr: T[]) => arr[Math.floor(Math.random() * arr.length)]
export const randomNumberBetween0and = (max: number) => Math.floor(Math.random() * max)

