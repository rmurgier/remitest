import { json } from '@remix-run/node'
import { DateTime } from 'luxon'

/**
 * This helper function helps us to return the accurate HTTP status,
 * 400 Bad Request, to the client.
 */
export const badRequest = <T>(data: T) => json<T>(data, { status: 400 })

export const validateString = (value: string) => {
	if (!value || value === '') {
		return 'You must at least write a little things'
	}
}

/**
 * Validate string array
 * the array must contain at least minEntry argument given
 */
type ValidateArrayPayload = {
	arr: string[]
	minimunEntry?: number
	input: 'Instruments' | 'Music styles'
}
const validateArray = ({ arr, minimunEntry = 0, input }: ValidateArrayPayload) => {
	if (!Array.isArray(arr)) {
		return `${input} must be a string array`
	}
	if (arr.length < minimunEntry) {
		return `You must pick at least one element from ${input}`
	}
}

const validateDate = (date: DateTime) => {
	if (!date.isValid) {
		return 'invalid date format'
	}
	// be gentle with user
	if (date < DateTime.now().minus({ minute: 1 })) {
		return "You can't pick an anterior date"
	}
}

const validateRadio = (value: unknown) => {
	if (!value) {
		return 'You must at least pick one musical style'
	}
}

export default {
	badRequest,
	validateString,
	validateArray,
	validateDate,
	validateRadio,
}