// Use enum for the test, in real case, use relational bdd => style: {id, name}
export enum MusicStyle {
	rock = 'Rock',
	metal = 'Metal',
	jpop = 'J-pop',
	classic = 'Classique',
}

export default MusicStyle

export const DEFAULT_MUSIC_STYLES: MusicStyle[] = [
	MusicStyle.metal,
	MusicStyle.rock,
	MusicStyle.jpop,
	MusicStyle.classic,
]

export const getDefaultMusicStyle = () => DEFAULT_MUSIC_STYLES
