// Use enum for the test, in real case, use relational bdd => city: {id, name}
export enum City {
	buckingham = 'Buckingham',
	chambery = 'Chambéry',
	bordeau = 'Bordeaux',
	voiron = 'Voiron',
}

export const DEFAULT_CITYS: City[] = [City.buckingham, City.chambery, City.bordeau, City.voiron]

export default interface Adress {
	city: City
	street: string
}

export const getDefaultCitys = () => DEFAULT_CITYS
