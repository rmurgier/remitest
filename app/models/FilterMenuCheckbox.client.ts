export interface ChekboxSectionOptions {
	value: string
	label: string
	checked: boolean
}

export interface CheckBoxSection<T> {
	id: keyof T
	name: string
	options: ChekboxSectionOptions[]
}
