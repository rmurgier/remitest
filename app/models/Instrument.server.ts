// Use enum for the test, in real case, use relational bdd => instrument: {id, name}
export enum Instrument {
	guitar = 'Guitare',
	bass = 'Bass',
	violin = 'Violon',
	piano = 'Piano',
}

export default Instrument

export const DEFAULT_INSTRUMENTS: Instrument[] = [
	Instrument.bass,
	Instrument.guitar,
	Instrument.violin,
	Instrument.piano,
]


export const getDefaultInstruments = () => DEFAULT_INSTRUMENTS

