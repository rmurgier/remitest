import fs from 'fs/promises'
import { MUSICIAN_DIR_PATH } from '~/constant.server'
import Instrument, { DEFAULT_INSTRUMENTS } from './Instrument.server'
import MusicStyle, { DEFAULT_MUSIC_STYLES } from './MusicStyle.server'
import FsService, { readFile } from '~/services/Fs.server'
import { createId, randomArrayElement } from '~/utils/utils.server'

// TODO add Affiliate Concert
export type Musician = {
	id: string
	instruments: Instrument[]
	musicStyle: MusicStyle[]
	pseudonym: string
	//concerts?: Concert[]
}

// Real case use cache

export const generateMusicans = async () => {
	const musicians = await getMusicians()
	if (musicians.length > 10) {
		throw new Error('Ever generate')
	}
	for (let i = 0; i < 20; i++) {
		const newMusician: Omit<Musician, 'id'> = {
			instruments: [randomArrayElement(DEFAULT_INSTRUMENTS)],
			musicStyle: [randomArrayElement(DEFAULT_MUSIC_STYLES)],
			pseudonym: `Musi${i + 1}`,
		}
		await createMusician(newMusician)
	}
	return
}

/**
 * Get musicians
 * if no musicians directory is found, create it
 */
export const getMusicians = async (): Promise<Musician[]> => {
	try {
		const filenames = await fs.readdir(MUSICIAN_DIR_PATH)
		const filePaths = filenames.map((name) => `${MUSICIAN_DIR_PATH}/${name}`)
		return await Promise.all(filePaths.map(readFile<Musician>))
	} catch (e) {
		const error = e as NodeJS.ErrnoException
		if (error.code && error.code === 'ENOENT') {
			await FsService.createDirectory(MUSICIAN_DIR_PATH)
			return []
		}
		throw e
	}
}

/**
 * Looking for musician by id
 */
export const getMusician = async (id: string): Promise<Musician> => {
	return await FsService.readFile(`${MUSICIAN_DIR_PATH}/${id}.json`)
}

/**
 * Create new Musician
 */
export const createMusician = async (musician: Omit<Musician, 'id'>) => {
	const id = createId()
	const newMusician = {
		id,
		...musician,
	}
	const filename = `${MUSICIAN_DIR_PATH}/${id}.json`
	await FsService.writeFile(filename, newMusician)
	return
}

/**
 * update new Musician id
 */
export const updateMusician = async (updatedMusician: Musician) => {
	const filename = `${MUSICIAN_DIR_PATH}/${updatedMusician.id}.json`
	await FsService.writeFile(filename, updatedMusician)
	return
}

/**
 * Remove Musician id
 */
export const deleteMusician = async (id: string) => {
	const filename = `${MUSICIAN_DIR_PATH}/${id}.json`
	await FsService.rmFile(filename)
	return
}


