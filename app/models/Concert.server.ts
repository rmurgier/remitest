import fs from 'fs/promises'
import { CONCERT_DIR_PATH } from '~/constant.server'
import MusicStyle, { DEFAULT_MUSIC_STYLES } from './MusicStyle.server'
import FsService, { readFile } from '~/services/Fs.server'
import { createId, randomArrayElement, randomNumberBetween0and } from '~/utils/utils.server'
import Adress, { DEFAULT_CITYS } from './Adress.server'
import { DateTime } from 'luxon'

export type Concert = {
	id: string
	musicStyle: MusicStyle
	address: Adress
	// iso string
	date: string
}

// with the time i think models is more like Manager , a model singleton class is a good alternative i think.

export const generateConcert = async () => {
	const concerts = await getConcerts()
	if (concerts.length > 10) {
		throw new Error('Ever generate')
	}
	for (let i = 0; i < 20; i++) {
		const dateArgs = {
			day: randomNumberBetween0and(90),
			hour: randomNumberBetween0and(23),
			minute: randomNumberBetween0and(45),
		}
		const newConcert: Omit<Concert, 'id'> = {
			address: {
				city: randomArrayElement(DEFAULT_CITYS),
				street: `Chemin de traverse ${i + 1}`,
			},
			date:
				i > 15
					? (DateTime.now().minus(dateArgs).toISO() as string)
					: (DateTime.now().plus(dateArgs).toISO() as string),
			musicStyle: randomArrayElement(DEFAULT_MUSIC_STYLES),
		}
		await createConcert(newConcert)
	}
	return
}

/**
 * Get concerts
 * if no Concerts directory is found, create it test only , onsafe to do that on prod
 */
export const getConcerts = async (): Promise<Concert[]> => {
	try {
		const filenames = await fs.readdir(CONCERT_DIR_PATH)
		const filePaths = filenames.map((name) => `${CONCERT_DIR_PATH}/${name}`)
		const concerts = await Promise.all(filePaths.map(readFile<Concert>))

		return concerts
			.filter((c) => DateTime.fromISO(c.date) > DateTime.now())
			.sort((a, b) => DateTime.fromISO(a.date).toMillis() - DateTime.fromISO(b.date).toMillis())
	} catch (e) {
		const error = e as NodeJS.ErrnoException
		if (error.code && error.code === 'ENOENT') {
			await FsService.createDirectory(CONCERT_DIR_PATH)
			return []
		}
		throw e
	}
}

/**
 * Looking for Convert by id
 */
export const getConcert = async (id: string): Promise<Concert> => {
	return await readFile(`${CONCERT_DIR_PATH}/${id}.json`)
}

/**
 * Create new Concert
 */
export const createConcert = async (concert: Omit<Concert, 'id'>) => {
	const id = createId()
	const newConcert = {
		id,
		...concert,
	}
	const filename = `${CONCERT_DIR_PATH}/${id}.json`
	await FsService.writeFile(filename, newConcert)
	return
}

/**
 * update new Concert id
 */
export const updateConcert = async (updatedConcert: Concert) => {
	const filename = `${CONCERT_DIR_PATH}/${updatedConcert.id}.json`
	await FsService.writeFile(filename, updatedConcert)
	return
}

/**
 * Remove Concert id
 */
export const deleteConcert = async (id: string) => {
	const filename = `${CONCERT_DIR_PATH}/${id}.json`
	await FsService.rmFile(filename)
	return
}
