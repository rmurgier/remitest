import { useMemo } from 'react'

export function useErrorString(error?: string | null | undefined) {
	return useMemo(() => {
		if (error) {
			return (
				<p className='text-red-500' role='alert'>
					{error}
				</p>
			)
		}
		return null
	}, [error])
}
