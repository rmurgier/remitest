import { useMemo } from 'react'
import { CheckBoxSection } from '~/models/FilterMenuCheckbox.client'

export type UseFilter = <T>(data: T[], filters: CheckBoxSection<T>[]) => T[]

export const useFilter: UseFilter = <T>(data: T[], filters: CheckBoxSection<T>[]): T[] => {
	return useMemo(() => {
		const enableFilters = filters.reduce<Record<string, string[]>>((result, next) => {
			return {
				...result,
				[next.id]: next.options.filter((opt) => opt.checked).map((opt) => opt.value),
			}
		}, {})

		const dataKey = Object.keys(enableFilters)

		return data.filter((d) => {
			// AND/OR
			// use a for because we want to break the loop if item match filters
			for (const key of dataKey) {
				const value = d[key as keyof T]
				const filterValues = enableFilters[key]

				// array property
				if (Array.isArray(value) && value.some((v) => filterValues.includes(v))) {
					return true
				}
				// string property
				if (typeof value === 'string' && filterValues.includes(value)) {
					return true
				}
			}

			return false
		})
	}, [data, filters])
}
