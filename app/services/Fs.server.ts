import fs from 'fs/promises'

/**
 * check if file content exist
 */
export const isExistPath = async (path: string): Promise<boolean> => {
	try {
		await fs.lstat(path)
		return true
	} catch {
		return false
	}
}
/**
 * Return the file content
 */
export const readFile = async <T>(path: string): Promise<T> => {
	const contentString = await fs.readFile(path, 'utf-8')
	return JSON.parse(contentString)
}

/**
 * create directory
 */
export const createDirectory = async (path: string) => {
	return await fs.mkdir(path, { recursive: true })
}

/**
 * write file
 */
export const writeFile = async <T>(path: string, data: T) => {
	const dataString = JSON.stringify(data, null, '\t')
	await fs.writeFile(path, dataString)
	console.log(`${path} updated`)
	return
}

/**
 * write file
 */
export const rmFile = async (path: string) => {
	await fs.rm(path)
	console.log(`${path} removed`)
	return
}

export default {
	readFile,
	createDirectory,
	writeFile,
	rmFile,
	isExistPath,
}
