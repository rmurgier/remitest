import { json } from '@remix-run/node'
import { useLoaderData, useNavigate } from '@remix-run/react'
import { useState } from 'react'
import FilterMenuCheckbox from '~/components/FilterMenuCheckbox'
import { CheckBoxSection, ChekboxSectionOptions } from '~/models/FilterMenuCheckbox.client'
import { getDefaultMusicStyle } from '~/models/MusicStyle.server'
import { CalendarIcon } from '@heroicons/react/20/solid'
import PageHead from '~/components/PageHead'
import { Concert, getConcerts } from '~/models/Concert.server'
import GridList from '~/components/gridLayout/GridList'
import GridListItem from '~/components/gridLayout/GridListItem'
import { DateTime } from 'luxon'
import Tag from '~/components/Tag'
import CustomButton from '~/components/CustomButton'
import { PencilIcon, PlusCircleIcon } from '@heroicons/react/24/outline'
import { useFilter } from '~/hook/useFilter'

export const loader = async () => {
	const musicStyles = getDefaultMusicStyle()
	const concerts = await getConcerts()
	const concertWithFormatedDate = concerts.map((c) => ({
		...c,
		date: DateTime.fromISO(c.date).toLocaleString(DateTime.DATETIME_MED, { locale: 'fr' }),
	}))
	return json({ concerts: concertWithFormatedDate, musicStyles })
}

const createChecboxOptions = (value: string): ChekboxSectionOptions => ({ value, label: value, checked: true })

export default function ConcertsIndex() {
	const { musicStyles, concerts } = useLoaderData<typeof loader>()
	const navigate = useNavigate()

	const [checkboxFilterSections, setCheckboxFilterSections] = useState<CheckBoxSection<Concert>[]>([
		{
			id: 'musicStyle',
			name: 'Style musical',
			options: musicStyles.map(createChecboxOptions),
		},
	])

	// TODO Remove dupe, create context, use context state, create generaique filter component
	const updateFilterValues = (index: number) => (optionsIndex: number, checked: boolean) => {
		setCheckboxFilterSections((prev) => {
			prev[index].options[optionsIndex].checked = checked
			return [...prev]
		})
	}

	const filteredConcert = useFilter(concerts, checkboxFilterSections)

	return (
		<div>
			<PageHead
				title='Concerts'
				subtitlElements={
					<div className='mt-1 flex flex-col sm:mt-0 sm:flex-row sm:flex-wrap sm:space-x-6'>
						<div className='mt-2 flex items-center text-sm text-gray-500'>
							<CalendarIcon className='mr-1.5 h-5 w-5 flex-shrink-0 text-gray-400' aria-hidden='true' />
							{concerts.length}
						</div>
					</div>
				}
				rightSideElements={
					<>
						<FilterMenuCheckbox sections={checkboxFilterSections} updateFilterValues={updateFilterValues} />
						<CustomButton
							name='Ajouter un concert'
							action={() => navigate('/concerts/new')}
							icon={
								<PlusCircleIcon className='-ml-0.5 mr-1.5 h-5 w-5 text-gray-400' aria-hidden='true' />
							}
						/>
					</>
				}
			/>
			<GridList>
				{filteredConcert.map((c) => (
					<GridListItem key={c.id}>
						<h3 className='font-extrabold'>{c.date}</h3>
						<h4 className='font-semibold text-sm'>{c.address.city}</h4>
						<p className='text-sm'>{c.address.street}</p>
						<h4 className='font-semibold text-sm mt-3'>Styles:</h4>
						<Tag bgColor='bg-green-50' textColor='text-green-700' text={c.musicStyle} />
						<div className='flex justify-end'>
							<CustomButton
								name='Edition'
								action={() => navigate(`/concerts/${c.id}`)}
								icon={
									<PencilIcon className='-ml-0.5 mr-1.5 h-5 w-5 text-gray-400' aria-hidden='true' />
								}
							/>
						</div>
					</GridListItem>
				))}
			</GridList>
		</div>
	)
}

export function ErrorBoundary() {
	return <div className='error-container'>I did a whoopsies.</div>
}
