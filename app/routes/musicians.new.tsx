import { ActionArgs, json, redirect } from '@remix-run/node'
import { useActionData, useLoaderData, useNavigation } from '@remix-run/react'
import { useMemo } from 'react'
import CheckboxInputs from '~/components/Form/CheckboxInputs'
import CustomForm from '~/components/Form/CustomForm'
import StringField from '~/components/Form/StringField'
import { getDefaultInstruments } from '~/models/Instrument.server'
import { getDefaultMusicStyle } from '~/models/MusicStyle.server'
import { Musician, createMusician } from '~/models/Musician.server'
import requestServer from '~/utils/request.server'

export const loader = async () => {
	const intruments = getDefaultInstruments()
	const musicStyles = getDefaultMusicStyle()
	return json({ intruments, musicStyles })
}

export const action = async ({ request }: ActionArgs) => {
	const form = await request.formData()
	const pseudonym = form.get('pseudonym')
	const instruments = form.getAll('instruments') as string[]
	const musicStyle = form.getAll('musicStyle') as string[]

	if (!Array.isArray(instruments) || !Array.isArray(musicStyle) || typeof pseudonym !== 'string') {
		return requestServer.badRequest({
			fieldErrors: null,
			fields: null,
			formError: 'Form not submitted correctly.',
		})
	}

	const fieldErrors = {
		pseudonym: requestServer.validateString(pseudonym),
		instruments: requestServer.validateArray({ arr: instruments, minimunEntry: 1, input: 'Instruments' }),
		musicStyle: requestServer.validateArray({ arr: musicStyle, minimunEntry: 1, input: 'Music styles' }),
	}

	const fields = { pseudonym, instruments, musicStyle }
	if (Object.values(fieldErrors).some(Boolean)) {
		return requestServer.badRequest({
			fieldErrors,
			fields,
			formError: null,
		})
	}

	await createMusician(fields as Omit<Musician, 'id'>)
	return redirect(`/musicians`)
}

export function ErrorBoundary() {
	return <div className='error-container'>Something unexpected went wrong. Sorry about that.</div>
}

export default function AddMusician() {
	const actionData = useActionData<typeof action>()
	const { intruments, musicStyles } = useLoaderData<typeof loader>()
	const navigation = useNavigation()
	const isPending = useMemo(() => navigation.state === 'submitting', [navigation])

	return (
		<CustomForm method='post' title='Ajouter un musicien' isPending={isPending} error={actionData?.formError}>
			<div className=' w-72'>
				<StringField
					label='Votre nom de scène'
					name='pseudonym'
					id='pseudonym'
					defaultValue={actionData?.fields?.pseudonym}
					error={actionData?.fieldErrors?.pseudonym}
				/>
			</div>
			<div className='flex align-top space-x-8 mt-5'>
				<CheckboxInputs
					legend='De quel instrument jouez-vous ?'
					nameAttribute='instruments'
					ids={intruments}
					error={actionData?.fieldErrors?.instruments}
					defaultValue={actionData?.fields?.instruments}
				/>
				<CheckboxInputs
					legend='Dans quel style musical jouez-vous ?'
					nameAttribute='musicStyle'
					ids={musicStyles}
					error={actionData?.fieldErrors?.musicStyle}
					defaultValue={actionData?.fields?.musicStyle}
				/>
			</div>
		</CustomForm>
	)
}
