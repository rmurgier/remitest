import { json } from '@remix-run/node'
import { useLoaderData, useNavigate } from '@remix-run/react'
import { useMemo, useState } from 'react'
import FilterMenuCheckbox from '~/components/FilterMenuCheckbox'
import PageHead from '~/components/PageHead'
import { CheckBoxSection, ChekboxSectionOptions } from '~/models/FilterMenuCheckbox.client'
import { getDefaultInstruments } from '~/models/Instrument.server'
import { getDefaultMusicStyle } from '~/models/MusicStyle.server'
import { Musician, getMusicians } from '~/models/Musician.server'
import { PencilIcon, PlusCircleIcon } from '@heroicons/react/24/outline'
import { UserIcon } from '@heroicons/react/24/solid'
import CustomButton from '~/components/CustomButton'
import Tag from '~/components/Tag'
import GridList from '~/components/gridLayout/GridList'
import GridListItem from '~/components/gridLayout/GridListItem'
import { useFilter } from '~/hook/useFilter'

export const loader = async () => {
	const intruments = getDefaultInstruments()
	const musicStyles = getDefaultMusicStyle()
	const musicians = await getMusicians()

	return json({ intruments, musicStyles, musicians })
}

const createChecboxOptions = (value: string): ChekboxSectionOptions => ({ value, label: value, checked: true })

export default function MusiciansIndex() {
	const { intruments, musicStyles, musicians } = useLoaderData<typeof loader>()
	const navigate = useNavigate()

	// use cache, can use local storage for persistence or move to back logic
	const [checkboxFilterSections, setCheckboxFilterSections] = useState<CheckBoxSection<Musician>[]>([
		{
			id: 'instruments',
			name: 'Instruments',
			options: intruments.map(createChecboxOptions),
		},
		{
			id: 'musicStyle',
			name: 'Style musical',
			options: musicStyles.map(createChecboxOptions),
		},
	])

	const filteredMusician = useFilter(musicians, checkboxFilterSections)

	const updateFilterValues = (index: number) => (optionsIndex: number, checked: boolean) => {
		setCheckboxFilterSections((prev) => {
			prev[index].options[optionsIndex].checked = checked
			return [...prev]
		})
	}

	return (
		<div className=''>
			<PageHead
				title='Musiciens'
				subtitlElements={
					<div className='mt-1 flex flex-col sm:mt-0 sm:flex-row sm:flex-wrap sm:space-x-6'>
						<div className='mt-2 flex items-center text-sm text-gray-500'>
							<UserIcon className='mr-1.5 h-5 w-5 flex-shrink-0 text-gray-400' aria-hidden='true' />
							{musicians.length}
						</div>
					</div>
				}
				rightSideElements={
					<>
						<FilterMenuCheckbox sections={checkboxFilterSections} updateFilterValues={updateFilterValues} />
						<CustomButton
							name='Ajouter un musicien'
							action={() => navigate('/musicians/new')}
							icon={
								<PlusCircleIcon className='-ml-0.5 mr-1.5 h-5 w-5 text-gray-400' aria-hidden='true' />
							}
						/>
					</>
				}
			/>
			<GridList>
				{filteredMusician.map((m) => (
					<GridListItem key={m.id}>
						<h3 className='font-extrabold'>{m.pseudonym}</h3>
						<h4 className='font-semibold text-sm mt-3'>Instruments:</h4>
						{m.instruments.map((instrument) => (
							<Tag
								key={instrument}
								bgColor='bg-purple-50'
								textColor='text-purple-700'
								text={instrument}
							/>
						))}
						<h4 className='font-semibold text-sm mt-3'>Styles:</h4>
						{m.musicStyle.map((style) => (
							<Tag key={style} bgColor='bg-green-50' textColor='text-green-700' text={style} />
						))}
						<div className='flex justify-end'>
							<CustomButton
								name='Edition'
								action={() => navigate(`/musicians/${m.id}`)}
								icon={
									<PencilIcon className='-ml-0.5 mr-1.5 h-5 w-5 text-gray-400' aria-hidden='true' />
								}
							/>
						</div>
					</GridListItem>
				))}
			</GridList>
		</div>
	)
}

export function ErrorBoundary() {
	return <div className='error-container'>I did a whoopsies.</div>
}
