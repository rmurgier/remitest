import { Outlet } from '@remix-run/react'

export default function Concerts() {
	return (
		<div className='h-full'>
			<Outlet />
		</div>
	)
}
