import { ActionArgs, LoaderArgs, json, redirect } from '@remix-run/node'
import { useActionData, useLoaderData, useNavigation } from '@remix-run/react'
import { DateTime } from 'luxon'
import { useMemo } from 'react'
import CustomForm from '~/components/Form/CustomForm'
import DateTimeField from '~/components/Form/DateTimeField'
import StringField from '~/components/Form/StringField'
import { City, getDefaultCitys } from '~/models/Adress.server'
import { Concert, createConcert, getConcert, updateConcert } from '~/models/Concert.server'
import MusicStyle, { getDefaultMusicStyle } from '~/models/MusicStyle.server'
import requestServer from '~/utils/request.server'

const inputDateFormat = "yyyy-LL-dd'T'HH:mm"
export const loader = async ({ params }: LoaderArgs) => {
	if (!params.id) {
		throw new Error('Invalid Id')
	}
	const musicStyles = getDefaultMusicStyle()
	const cities = getDefaultCitys()
	const minDate = DateTime.now().toFormat(inputDateFormat)
	const concert = await getConcert(params.id)
	if (!concert) {
		throw new Error('Concert not found')
	}
	const formatedConcertDateForInput = DateTime.fromISO(concert.date).toFormat(inputDateFormat)
	return json({ musicStyles, cities, minDate, concert, formatedConcertDateForInput })
}

export const action = async ({ request, params }: ActionArgs) => {
	if (!params.id) {
		throw new Error('Invalid Id')
	}
	const form = await request.formData()
	const date = form.get('date')
	const city = form.get('city') as City
	const street = form.get('street') as string
	const musicStyle = form.get('musicStyle') as MusicStyle

	const fieldErrors: Record<string, undefined | null | string> = {
		date: undefined,
		city: undefined,
		street: undefined,
		musicStyle: requestServer.validateRadio(musicStyle),
	}

	let luxonDate: DateTime | undefined = undefined
	if (typeof date === 'string' && date !== '') {
		luxonDate = DateTime.fromFormat(date, inputDateFormat)
		fieldErrors.date = requestServer.validateDate(luxonDate)
	}
	if (typeof street === 'string') {
		fieldErrors.street = requestServer.validateString(street)
	}
	if (typeof city === 'string') {
		fieldErrors.city = requestServer.validateString(city)
	}

	const fields = { city, musicStyle, date, street }
	if (Object.values(fieldErrors).some(Boolean)) {
		return requestServer.badRequest({
			fieldErrors,
			fields,
			formError: null,
		})
	}

	const newConcert: Concert = {
		id: params.id,
		date: (luxonDate && luxonDate.toISO()) as string,
		musicStyle,
		address: {
			city,
			street,
		},
	}
	await updateConcert(newConcert)
	return redirect(`/concerts`)
}

export function ErrorBoundary() {
	return <div className='error-container'>Something unexpected went wrong. Sorry about that.</div>
}

export default function Concert() {
	const actionData = useActionData<typeof action>()
	const { musicStyles, cities, minDate, concert, formatedConcertDateForInput } = useLoaderData<typeof loader>()
	const navigation = useNavigation()
	const isPending = useMemo(() => navigation.state === 'submitting', [navigation])

	// use the orginal method to keep select value but a little less stable
	const selecterOpts = useMemo(() => {
		return cities.map((c) => (
			<option key={c} value={c}>
				{c}
			</option>
		))
	}, [cities])

	return (
		<CustomForm method='post' title={`Editer le concert`} isPending={isPending} error={actionData?.formError}>
			<div className=' w-72 mb-5'>
				<DateTimeField
					label='Choisir une date pour le concert'
					name='date'
					id='date'
					defaultValue={actionData?.fields?.date || formatedConcertDateForInput || minDate}
					error={actionData?.fieldErrors?.date}
					minDate={minDate}
				/>
			</div>
			<div className='flex gap-4'>
				<div className='sm:col-span-3'>
					<label htmlFor='country' className='block text-sm font-medium leading-6 text-gray-900'>
						Ville
					</label>
					<div className='mt-2'>
						<select
							id='city'
							name='city'
							autoComplete='city-name'
							defaultValue={actionData?.fields?.city || concert.address.city}
							className='block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6'
						>
							{selecterOpts}
						</select>
					</div>
				</div>
				<div className=' w-72'>
					<StringField
						label='Rue'
						name='street'
						id='street'
						defaultValue={actionData?.fields?.street || concert.address.street}
						error={actionData?.fieldErrors?.street}
					/>
				</div>
			</div>

			{/* Another Error management */}
			<div className='flex align-top space-x-8 mt-5'>
				<fieldset>
					<legend
						className={`text-sm font-semibold leading-6 ${
							actionData?.fieldErrors?.musicStyle ? 'text-red-500' : 'text-gray-900'
						}`}
					>
						{actionData?.fieldErrors?.musicStyle || 'Style musical du concert'}
					</legend>
					<div className='mt-3'>
						{musicStyles.map((s) => (
							<div key={s}>
								<div className='flex items-center gap-x-3'>
									<input
										id={s}
										name={'musicStyle'}
										type='radio'
										value={s}
										defaultChecked={
											s === actionData?.fields?.musicStyle || concert.musicStyle === s
										}
										className='h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-600'
									/>
									<label
										htmlFor='push-everything'
										className='block text-sm font-medium leading-6 text-gray-900'
									>
										{s}
									</label>
								</div>
							</div>
						))}
					</div>
				</fieldset>
			</div>
		</CustomForm>
	)
}
