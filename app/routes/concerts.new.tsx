import { ActionArgs, json, redirect } from '@remix-run/node'
import { useActionData, useLoaderData, useNavigation } from '@remix-run/react'
import { DateTime } from 'luxon'
import { useMemo } from 'react'
import CustomForm from '~/components/Form/CustomForm'
import DateTimeField from '~/components/Form/DateTimeField'
import StringField from '~/components/Form/StringField'
import { City, getDefaultCitys } from '~/models/Adress.server'
import { Concert, createConcert } from '~/models/Concert.server'
import MusicStyle, { getDefaultMusicStyle } from '~/models/MusicStyle.server'
import requestServer from '~/utils/request.server'

const inputDateFormat = "yyyy-LL-dd'T'HH:mm"
export const loader = async () => {
	const musicStyles = getDefaultMusicStyle()
	const cities = getDefaultCitys()
	const minDate = DateTime.now().toFormat(inputDateFormat)
	return json({ musicStyles, cities, minDate })
}

export const action = async ({ request }: ActionArgs) => {
	const form = await request.formData()
	const date = form.get('date')
	const city = form.get('city') as City
	const street = form.get('street')
	const musicStyle = form.get('musicStyle') as MusicStyle

	if (typeof date !== 'string' || typeof city !== 'string' || typeof street !== 'string') {
		return requestServer.badRequest({
			fieldErrors: null,
			fields: null,
			formError: 'Form not submitted correctly.',
		})
	}

	const luxonDate = DateTime.fromFormat(date, inputDateFormat)
	const fieldErrors = {
		date: requestServer.validateDate(luxonDate),
		city: requestServer.validateString(city),
		street: requestServer.validateString(street),
		musicStyle: requestServer.validateRadio(musicStyle),
	}

	const fields = { city, musicStyle, date, street }
	if (Object.values(fieldErrors).some(Boolean)) {
		return requestServer.badRequest({
			fieldErrors,
			fields,
			formError: null,
		})
	}

	const newConcert: Omit<Concert, 'id'> = {
		// can't be null with the validation just before
		date: luxonDate.toISO() as string,
		musicStyle,
		address: {
			city,
			street,
		},
	}
	await createConcert(newConcert)
	return redirect(`/concerts`)
}

export function ErrorBoundary() {
	return <div className='error-container'>Something unexpected went wrong. Sorry about that.</div>
}

export default function AddConcert() {
	const actionData = useActionData<typeof action>()
	const { musicStyles, cities, minDate } = useLoaderData<typeof loader>()
	const navigation = useNavigation()
	const isPending = useMemo(() => navigation.state === 'submitting', [navigation])

	// use the orginal method to keep select value but a little less stable
	const selecterOpts = useMemo(() => {
		return cities.map((c) => (
			<option key={c} value={c}>
				{c}
			</option>
		))
	}, [cities])

	return (
		<CustomForm method='post' title='Ajouter un concert' isPending={isPending} error={actionData?.formError}>
			<div className=' w-72 mb-5'>
				<DateTimeField
					label='Choisir une date pour le concert'
					name='date'
					id='date'
					defaultValue={actionData?.fields?.date || minDate}
					error={actionData?.fieldErrors?.date}
					minDate={minDate}
				/>
			</div>
			<div className='flex gap-4'>
				<div className='sm:col-span-3'>
					<label htmlFor='country' className='block text-sm font-medium leading-6 text-gray-900'>
						Ville
					</label>
					<div className='mt-2'>
						<select
							id='city'
							name='city'
							autoComplete='city-name'
							defaultValue={actionData?.fields?.city}
							className='block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6'
						>
							{selecterOpts}
						</select>
					</div>
				</div>
				<div className=' w-72'>
					<StringField
						label='Rue'
						name='street'
						id='street'
						defaultValue={actionData?.fields?.street}
						error={actionData?.fieldErrors?.street}
					/>
				</div>
			</div>

			{/* Another Error management */}
			<div className='flex align-top space-x-8 mt-5'>
				<fieldset>
					<legend
						className={`text-sm font-semibold leading-6 ${
							actionData?.fieldErrors?.musicStyle ? 'text-red-500' : 'text-gray-900'
						}`}
					>
						{actionData?.fieldErrors?.musicStyle || 'Style musical du concert'}
					</legend>
					<div className='mt-3'>
						{musicStyles.map((s) => (
							<div key={s}>
								<div className='flex items-center gap-x-3'>
									<input
										id={s}
										name={'musicStyle'}
										type='radio'
										value={s}
										className='h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-600'
									/>
									<label
										htmlFor='push-everything'
										className='block text-sm font-medium leading-6 text-gray-900'
									>
										{s}
									</label>
								</div>
							</div>
						))}
					</div>
				</fieldset>
			</div>
		</CustomForm>
	)
}
