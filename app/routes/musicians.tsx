import { Outlet } from '@remix-run/react'

export default function Musicians() {
	return (
		<div className='h-full'>
			<Outlet />
		</div>
	)
}
