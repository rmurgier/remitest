import { isRouteErrorResponse, useActionData, useNavigation, useParams, useRouteError } from '@remix-run/react'
import { ActionArgs, LoaderArgs, json, redirect } from '@remix-run/node'
import { useLoaderData } from '@remix-run/react'

import { Musician, getMusician, updateMusician } from '~/models/Musician.server'
import requestServer from '~/utils/request.server'
import CustomForm from '~/components/Form/CustomForm'
import StringField from '~/components/Form/StringField'
import CheckboxInputs from '~/components/Form/CheckboxInputs'
import { getDefaultInstruments } from '~/models/Instrument.server'
import { getDefaultMusicStyle } from '~/models/MusicStyle.server'
import { useMemo } from 'react'

// well why not ? the doc say it
export function ErrorBoundary() {
	const error = useRouteError()

	if (isRouteErrorResponse(error)) {
		return (
			<div>
				<h1>
					{error.status} {error.statusText}
				</h1>
				<p>{error.data}</p>
			</div>
		)
	} else if (error instanceof Error) {
		return (
			<div>
				<h1>Error</h1>
				<p>{error.message}</p>
			</div>
		)
	} else {
		return <div>There was an error loading Musician. Sorry.</div>
	}
}

export const action = async ({ request, params }: ActionArgs) => {
	if (!params.id) {
		throw new Error('Invalid Id')
	}
	const form = await request.formData()
	const pseudonym = form.get('pseudonym')
	const instruments = form.getAll('instruments') as string[]
	const musicStyle = form.getAll('musicStyle') as string[]

	if (!Array.isArray(instruments) || !Array.isArray(musicStyle)) {
		return requestServer.badRequest({
			fieldErrors: null,
			fields: null,
			formError: 'Form not submitted correctly.',
		})
	}

	const fieldErrors: Record<string, undefined | null | string> = {
		pseudonym: undefined,
		instruments: requestServer.validateArray({ arr: instruments, minimunEntry: 1, input: 'Instruments' }),
		musicStyle: requestServer.validateArray({ arr: musicStyle, minimunEntry: 1, input: 'Music styles' }),
	}
	if (typeof pseudonym === 'string' && pseudonym !== '') {
		fieldErrors.pseudonym = requestServer.validateString(pseudonym)
	}

	const fields = { pseudonym, instruments, musicStyle }
	if (Object.values(fieldErrors).some(Boolean)) {
		return requestServer.badRequest({
			fieldErrors,
			fields,
			formError: null,
		})
	}
	const newMusician = {
		id: params.id,
		...fields,
	} as Musician

	await updateMusician(newMusician)
	return redirect(`/musicians`)
}

export const loader = async ({ params }: LoaderArgs) => {
	if (!params.id) {
		throw new Error('Invalid Id')
	}
	const musician = await getMusician(params.id)
	if (!musician) {
		throw new Error('Musician not found')
	}
	const intruments = getDefaultInstruments()
	const musicStyles = getDefaultMusicStyle()

	return json({ musician, intruments, musicStyles })
}

export default function Musician() {
	const actionData = useActionData<typeof action>()
	const { musician, intruments, musicStyles } = useLoaderData<typeof loader>()
	const navigation = useNavigation()
	const isPending = useMemo(() => navigation.state === 'submitting', [navigation])

	return (
		<CustomForm
			method='post'
			title={`Editer le musicien ${musician.pseudonym}`}
			isPending={isPending}
			error={actionData?.formError}
			buttonName='Modifier'
		>
			<div className=' w-72'>
				<StringField
					label='Votre nom de scène'
					name='pseudonym'
					id='pseudonym'
					defaultValue={actionData?.fields?.pseudonym || musician.pseudonym}
					error={actionData?.fieldErrors?.pseudonym}
				/>
			</div>
			<div className='flex align-top space-x-8 mt-5'>
				<CheckboxInputs
					legend='De quel instrument jouez-vous ?'
					nameAttribute='instruments'
					ids={intruments}
					error={actionData?.fieldErrors?.instruments}
					defaultValue={actionData?.fields?.instruments || musician.instruments}
				/>
				<CheckboxInputs
					legend='Dans quel style musical jouez-vous ?'
					nameAttribute='musicStyle'
					ids={musicStyles}
					error={actionData?.fieldErrors?.musicStyle}
					defaultValue={actionData?.fields?.musicStyle || musician.musicStyle}
				/>
			</div>
		</CustomForm>
	)
}
