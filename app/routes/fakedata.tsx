import { json, redirect } from '@remix-run/node'
import { generateConcert } from '~/models/Concert.server'
import { generateMusicans } from '~/models/Musician.server'

export async function action() {
	try {
		await generateConcert()
		await generateMusicans()
		return json({ error: null, success: true })
	} catch (error) {
		return redirect('/')
	}
}
