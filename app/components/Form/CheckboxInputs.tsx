import { useMemo } from 'react'
import { useErrorString } from '~/hook/useErrorString'

/**
 * use the getAll operation on name attribute for take all checkbox values
 */
interface CheckboxInputProps {
	legend: string
	nameAttribute: string
	ids: string[]
	error?: string | null | undefined
	defaultValue?: string[]
}

export default function CheckboxInputs({ legend, nameAttribute, ids, error, defaultValue = [] }: CheckboxInputProps) {
	const errorString = useErrorString(error)

	const inputs = useMemo(() => {
		return ids.map((id) => (
			<div key={id} className='relative flex gap-x-3'>
				<div className='flex items-center'>
					<input
						// maybe use unique id for prevent incorecct behavior ?
						id={id}
						className='h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600'
						name={nameAttribute}
						type='checkbox'
						value={id}
						defaultChecked={defaultValue.includes(id)}
					/>
				</div>
				<div className='text-sm'>
					<label htmlFor={id} className='font-medium text-gray-900'>
						{id}
					</label>
				</div>
			</div>
		))
	}, [nameAttribute, ids])

	return (
		<div className='rounded-md bg-white p-5'>
			<fieldset>
				<legend className='text-sm font-semibold leading-6 text-gray-900'>{legend}</legend>
				<div className='mt-3'>{inputs}</div>
				<div className='mt-3'>{errorString}</div>
			</fieldset>
		</div>
	)
}
