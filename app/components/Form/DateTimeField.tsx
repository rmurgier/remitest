import { useErrorString } from '~/hook/useErrorString'

type StringFieldProps = {
	label: string
	name: string
	defaultValue?: string
	id: string
	inputClassName?: string
	error?: string | null | undefined
	minDate: string
}

const defaultInputClassName = `block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6`

const DateTimeField = ({
	label,
	name,
	defaultValue,
	id,
	error,
	inputClassName = defaultInputClassName,
	minDate,
}: StringFieldProps) => {
	const errorString = useErrorString(error)

	return (
		<div className='w-full'>
			<div className='sm:col-span-4'>
				<label htmlFor={id} className='block text-sm font-semibold leading-6 text-gray-900'>
					{label} :
				</label>
				<div className='mt-2'>
					<div className='flex bg-white rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md'>
						<input
							id={id}
							defaultValue={defaultValue}
							type='datetime-local'
							name={name}
							aria-invalid={Boolean(error)}
							aria-errormessage={error ? `${name}-error` : undefined}
							className={inputClassName}
							min={minDate}
						/>
					</div>
				</div>
			</div>
			{errorString}
		</div>
	)
}

export default DateTimeField
