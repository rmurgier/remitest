import { useNavigate } from '@remix-run/react'
import { MouseEvent, ReactNode, useMemo } from 'react'
import { useErrorString } from '~/hook/useErrorString'

type CustomFormProps = {
	children: ReactNode
	method: 'get' | 'post'
	actionUrl?: string
	buttonName?: string
	buttonClassName?: string
	title: string
	isPending: boolean
	pendingString?: string
	error?: string | null | undefined
}

const defaultButtonClassName =
	'rounded bg-green-500 py-2 px-4 text-white hover:bg-green-600 focus:bg-green-400 disabled:bg-green-300'

// TODO use infinite progress en pending
export const CustomForm = ({
	children,
	method,
	actionUrl,
	buttonName = 'Ajouter',
	buttonClassName = defaultButtonClassName,
	title = 'Ajouter',
	isPending,
	pendingString = 'En cours...',
	error,
}: CustomFormProps) => {
	const errorString = useErrorString(error)
	const navigate = useNavigate()

	const goBack = (e: MouseEvent) => {
		e.preventDefault()
		e.stopPropagation()
		navigate(-1)
	}

	return (
		<>
			<p className='font-semibold text-2xl mb-8'>{title}</p>
			<form method={method} action={actionUrl}>
				{children}
				{errorString}
				<div className='mt-5 space-x-4'>
					<button type='submit' className={buttonClassName} disabled={isPending}>
						{isPending ? pendingString : buttonName}
					</button>
					<button
						onClick={goBack}
						className='rounded bg-orange-500 py-2 px-4 text-white hover:bg-orange-300 focus:bg-orange-400 disabled:bg-orange-300'
						disabled={isPending}
					>
						Annuler
					</button>
				</div>
			</form>
		</>
	)
}

export default CustomForm
