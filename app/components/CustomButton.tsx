import { ReactNode } from 'react'

type CustomButtonProps = {
	icon: ReactNode
	action: () => void
	name: string
	buttonClassName?: string
	additionalClass?: string
}

const defaultButtonClassName =
	'inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50'

export const CustomButton = ({
	icon,
	action,
	name,
	buttonClassName = defaultButtonClassName,
	additionalClass,
}: CustomButtonProps) => {
	return (
		<button type='button' className={`${buttonClassName} ${additionalClass}`} onClick={() => action()}>
			{icon}
			{name}
		</button>
	)
}

export default CustomButton
