import { useMemo } from 'react'

interface TagProps {
	bgColor?: string
	textColor?: string
	ringColor?: string
	additionalClass?: string
	text: string
}

export default function Tag({ bgColor, textColor, ringColor, text, additionalClass }: TagProps) {
	const className = useMemo(() => {
		let defaultClass = 'inline-flex items-center rounded-md px-2 py-1 text-xs font-medium ring-1 ring-inset m-1'
		defaultClass += bgColor ? ` ${bgColor}` : ' bg-gray-50'
		defaultClass += textColor ? ` ${textColor}` : ' text-gray-600'
		defaultClass += ringColor ? ` ${ringColor}` : ' ring-gray-500/10'
		if (additionalClass) {
			defaultClass += ` ${additionalClass}`
		}
		return defaultClass
	}, [bgColor, textColor])

	return <span className={className}>{text}</span>
}
