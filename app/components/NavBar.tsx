import { useEffect, useMemo, useState } from 'react'
import { Disclosure } from '@headlessui/react'
import { MusicalNoteIcon } from '@heroicons/react/24/outline'
import { useFetcher, useLocation } from '@remix-run/react'

type NavLink = { name: string; href: string; current: boolean }
const DefaultLink: NavLink[] = [
	{ name: 'Accueil', href: '/', current: true },
	{ name: 'Musiciens', href: '/musicians', current: false },
	{ name: 'Concerts', href: '/concerts', current: false },
]

function classNames(...classes: string[]) {
	return classes.filter(Boolean).join(' ')
}

export default function NavBar() {
	const { pathname } = useLocation()
	const fetcher = useFetcher()
	const [successFlag, setsuccessFlag] = useState<boolean>(false)
	const links = useMemo(() => {
		return DefaultLink.map((link) => ({
			...link,
			current: link.href === pathname,
		}))
	}, [pathname])

	const onClick = () => {
		fetcher.submit({}, { method: 'post', action: '/fakedata' })
	}

	useEffect(() => {
		const { data } = fetcher
		if (data && data.success && !successFlag) {
			setsuccessFlag(true)
			alert('Vos données on été générées, recharcher la page si vous ne les voyez pas')
		}
	}, [fetcher, successFlag])

	return (
		<nav className='bg-gray-800'>
			<div className='mx-auto max-w-7xl px-2 sm:px-6 lg:px-8'>
				<div className='relative flex h-16 items-center justify-between'>
					<div className='flex flex-1 items-center lg:justify-center sm:justify-start sm:items-stretch'>
						<div className='flex items-center'>
							<MusicalNoteIcon
								className='h-8 w-auto lg:block mr-5 hover:cursor-pointer'
								color='white'
								onClick={onClick}
							/>
						</div>
						<div className='flex space-x-4'>
							{links.map((item, index) => (
								<a
									key={item.name}
									href={item.href}
									className={classNames(
										item.current
											? 'bg-gray-200 text-black'
											: 'text-gray-300 hover:bg-gray-700 hover:text-white',
										'rounded-md px-3 py-2 text-sm font-medium'
									)}
									aria-current={item.current ? 'page' : undefined}
								>
									{item.name}
								</a>
							))}
						</div>
					</div>
				</div>
			</div>
		</nav>
	)
}
