import { Disclosure } from '@headlessui/react'
import { MinusIcon, PlusIcon } from '@heroicons/react/24/outline'
import { CheckBoxSection } from '~/models/FilterMenuCheckbox.client'
import { Popover, Transition } from '@headlessui/react'
import { ChevronDownIcon } from '@heroicons/react/20/solid'
import { FormEvent, Fragment } from 'react'

interface CheckboxMenuProps<T> {
	sections: CheckBoxSection<T>[]
	updateFilterValues: (sectionPosition: number) => (optionPosition: number, isCheck: boolean) => void
}

export default function FilterMenuCheckbox<T>({ sections, updateFilterValues }: CheckboxMenuProps<T>) {
	const onchange = (e: FormEvent<HTMLInputElement>, sectionIndex: number, optionIndex: number) => {
		updateFilterValues(sectionIndex)(optionIndex, e.currentTarget.checked)
	}
	return (
		<Popover className='relative'>
			{({ open }) => (
				<>
					<Popover.Button
						className={`
                ${open ? '' : 'text-opacity-90'}
                group inline-flex items-center rounded-md bg-white px-3 py-2 text-base font-semibold text-gray-900 shadow-sm hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 hover:bg-gray-300 ring-1 ring-inset ring-gray-300`}
					>
						<span>Filtres</span>
						<ChevronDownIcon
							className={`${open ? '' : 'text-opacity-70'}
                  ml-2 h-5 w-5 text-grey-900 transition duration-150 ease-in-out group-hover:text-opacity-80`}
							aria-hidden='true'
						/>
					</Popover.Button>
					<Transition
						as={Fragment}
						enter='transition ease-out duration-200'
						enterFrom='opacity-0 translate-y-1'
						enterTo='opacity-100 translate-y-0'
						leave='transition ease-in duration-150'
						leaveFrom='opacity-100 translate-y-0'
						leaveTo='opacity-0 translate-y-1'
					>
						<Popover.Panel className='absolute left-1/2 z-10 mt-3 w-screen max-w-sm -translate-x-1/2 transform px-4 sm:px-0 lg:max-w-3xl bg-white'>
							{sections.map((section, sectionIndex) => (
								<Disclosure
									as='div'
									key={section.name + sectionIndex}
									className='border-t border-gray-200 px-4 py-6'
								>
									{({ open }) => (
										<>
											<h3 className='-mx-2 -my-3 flow-root'>
												<Disclosure.Button className='flex w-full items-center bg-white px-2 py-3 text-gray-400 hover:text-gray-500'>
													<span className='ml-6 flex items-center mr-5'>
														{open ? (
															<MinusIcon className='h-5 w-5' aria-hidden='true' />
														) : (
															<PlusIcon className='h-5 w-5' aria-hidden='true' />
														)}
													</span>
													<span className='font-medium text-gray-900'>{section.name}</span>
												</Disclosure.Button>
											</h3>
											<Disclosure.Panel className='pt-6'>
												<div className='space-y-6'>
													{section.options.map((option, optionIndex) => (
														<div key={option.value} className='flex items-center'>
															<input
																id={`filter-mobile-${section.name}-${optionIndex}`}
																name={section.name}
																defaultValue={option.value}
																type='checkbox'
																defaultChecked={option.checked}
																className='h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500'
																onChange={(e) => onchange(e, sectionIndex, optionIndex)}
															/>
															<label
																htmlFor={`filter-mobile-${section.name}-${optionIndex}`}
																className='ml-3 min-w-0 flex-1 text-gray-500'
															>
																{option.label}
															</label>
														</div>
													))}
												</div>
											</Disclosure.Panel>
										</>
									)}
								</Disclosure>
							))}
						</Popover.Panel>
					</Transition>
				</>
			)}
		</Popover>
	)
}
