import { ReactNode } from 'react'
import { TrashIcon, PencilIcon } from '@heroicons/react/20/solid'
import CustomButton from './CustomButton'

const removeButton = (
	<CustomButton
		name='Supprimer'
		action={() => alert('wellDone')}
		icon={<TrashIcon className='-ml-0.5 mr-1.5 h-5 w-5 text-gray-200' aria-hidden='true' />}
		additionalClass='bg-red-700 text-gray-200 hover:bg-red-500'
	/>
)

interface PageHeadProps {
	title: string
	subtitlElements?: ReactNode
	rightSideElements?: ReactNode
}

export default function PageHead({ title, subtitlElements, rightSideElements }: PageHeadProps) {
	return (
		<div className='lg:flex lg:items-center lg:justify-between border-b border-gray-300 pb-6'>
			<div className='min-w-0 flex-1'>
				<h2 className='text-2xl font-bold leading-7 text-gray-900 sm:truncate sm:text-3xl sm:tracking-tight'>
					{title}
				</h2>
				{subtitlElements}
			</div>
			<div className='mt-5 flex lg:ml-4 lg:mt-0 space-x-2'>{rightSideElements}</div>
		</div>
	)
}
