import { ReactNode } from 'react'

interface GridListProps {
	children: ReactNode
	tailwinsGridColClass?: string
	tailwinsGridItemGap?: string
}

export default function GridList({
	children,
	tailwinsGridColClass = 'grid-cols-4',
	tailwinsGridItemGap = 'gap-3',
}: GridListProps) {
	return <div className={`grid ${tailwinsGridColClass} ${tailwinsGridItemGap} pb-24`}>{children}</div>
}
