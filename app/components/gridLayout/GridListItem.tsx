import { ReactNode } from 'react'

interface GridListItemProps {
	children: ReactNode
}

export default function GridListItem({ children }: GridListItemProps) {
	return <div className='w-full rounded-md bg-gray-200 p-4 mt-5 shadow-md'>{children}</div>
}
