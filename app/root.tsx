import { LiveReload, Outlet, Links, useRouteError, Scripts, ScrollRestoration, Meta } from '@remix-run/react'
import type { LinksFunction } from '@remix-run/node'
import type { PropsWithChildren } from 'react'
import NavBar from './components/NavBar'

import styles from './tailwind.config.css'

export const links: LinksFunction = () => [{ rel: 'stylesheet', href: styles }]

function Document({ children, title = 'Remi X test' }: PropsWithChildren<{ title?: string }>) {
	return (
		<html lang='fr'>
			<head>
				<meta charSet='utf-8' />
				<meta name='viewport' content='width=device-width,initial-scale=1' />
				<Meta />
				<title>{title}</title>
				<Links />
				<ScrollRestoration />
			</head>
			<body className='h-screen overflow-hidden'>
				<Scripts />
				{children}
				<LiveReload />
			</body>
		</html>
	)
}

export default function App() {
	return (
		<Document>
			<NavBar />
			<main className='h-full w-full p-5 bg-gray-100 overflow-auto'>
				<Outlet />
			</main>
		</Document>
	)
}

export function ErrorBoundary() {
	const error = useRouteError()
	const errorMessage = error instanceof Error ? error.message : 'Unknown error'
	return (
		<Document title='Uh-oh!'>
			<div className='error-container'>
				<h1>App Error</h1>
				<pre>{errorMessage}</pre>
			</div>
		</Document>
	)
}
